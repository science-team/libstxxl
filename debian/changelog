libstxxl (1.4.1-4) unstable; urgency=medium

  * [fe2eba4] Add .gitlab-ci.yml.
  * [3c8b84c] Disable pdf build. (Closes: #1051165)
  * [6a530dc] Trim trailing whitespace.
  * [2bf9fcd] Update watch file format version to 4.
  * [aeff07a] Bump debhelper from old 11 to 13.
  * [48b1e14] Set debhelper-compat version in Build-Depends.
  * [0151f9e] Fix day-of-week for changelog entry 1.3.1-4.
  * [2aa4f8a] Remove field Priority on binary packages
              libstxxl-dev, libstxxl-doc, libstxxl1-bin that duplicates source.
  * [7ec46e6] Set Recommends-Versions to 4.6.2.
  * [5ccf28b] Apply cme fix dpkg.
  * [45b9d3f] Add myself as uploader.

 -- Anton Gladky <gladk@debian.org>  Fri, 15 Sep 2023 19:36:33 +0200

libstxxl (1.4.1-3) unstable; urgency=medium

  * Team upload.
  * [973f16b] Switch to compat level 11
  * [76a40a5] Update VCS-fields
  * [d0e648e] Remove outdated Testsuite-field
  * [c26df19] Set Standards-Version: 4.1.3
  * [8973044] Apply cme fix dpkg
  * [c3b6b0a] Replace priority extra by optional
  * [832b6c2] Drop dbg-package
  * [de76517] Remove myself from uploaders

 -- Anton Gladky <gladk@debian.org>  Thu, 08 Mar 2018 21:01:26 +0100

libstxxl (1.4.1-2) unstable; urgency=medium

  [ Anton Gladky ]
  * [10bbced] Apply cme fix dpkg-control.
  * [2da8441] Add a description to patch.
  * [c9f296e] Remove unneded lintian-overrides.
  * [23d7673] Rename shared lib due to GCC-5 transition. (Closes: #791173)
  * [0105031] Use DEP-5 for d/copyright.

  [ Bas Couwenberg ]
  * Use versioned Breaks/Replaces.

 -- Anton Gladky <gladk@debian.org>  Sun, 23 Aug 2015 17:00:29 +0200

libstxxl (1.4.1-1) unstable; urgency=medium

  [ D Haley ]
  * [7d8f358] Imported Upstream version 1.4.1
  * [69811b5] Refresh patches
  * [d8eca54] Update standards version

  [ Anton Gladky ]
  * [066a810] Fix bugnumber.
  * [5e7edbb] Remove timestamp from HTML doxygen build.

 -- Anton Gladky <gladk@debian.org>  Sun, 17 May 2015 22:18:32 +0200

libstxxl (1.4.0-3) unstable; urgency=medium

  [ Anton Gladky ]
  * [799da84] Add myself to uploaders.
  * [e56aaad] Add autopkgtests.
  * [76dc00f] Simplify debian/rules.
  * [af58c7f] Use wrap-and-sort.

  [ D Haley ]
  * [3d9d2d1] Fix FTBFS for 32 bit via upstream patches (Closes: #759701)
  * [44d06b0] Relink against system jquery
  * [2618c7f] Remove testing ccache CC/CXX

 -- Anton Gladky <gladk@debian.org>  Sun, 21 Sep 2014 10:39:51 +0200

libstxxl (1.4.0-2) unstable; urgency=medium

  * Team upload.
  * [f08e83e] Revert unstable back to experimental.
  * [1042133] Set canonical VCS-field.
  * [9deb646] Set Standards-Version: 3.9.5. No changes.

 -- Anton Gladky <gladk@debian.org>  Thu, 28 Aug 2014 21:57:17 +0200

libstxxl (1.4.0-1) experimental; urgency=low

  * Update to new upstream

 -- D Haley <mycae@gmx.com>  Sun, 15 Dec 2013 23:44:00 +0100

libstxxl (1.3.1-5) unstable; urgency=low

  * Apply doxygen SVG output patch (Closes: #721261)
  * Enable hardening during build
  * Update uploader email

 -- D Haley <mycae@gmx.com>  Fri, 30 Aug 2013 22:34:04 +0200

libstxxl (1.3.1-4) unstable; urgency=low

  * Remove ICPC compat header (Closes: #674779)

 -- D Haley <mycae@yahoo.com>  Sun, 17 Jun 2012 04:34:32 +1000

libstxxl (1.3.1-3) unstable; urgency=low

  * Team upload.
  * Fix build failure with GCC 4.7, thanks to Matthias Klose (Closes: #672055)
  * Bump standards version to 3.9.3
  * Remove useless quilt references in debian/control & debian/rules.
  * Remove duplicate section in debian/control.

 -- Dmitrijs Ledkovs <xnox@debian.org>  Sun, 27 May 2012 14:44:51 +0100

libstxxl (1.3.1-2) unstable; urgency=low

  * Team upload
  * Standards-Version updated to version 3.9.2
  * Remove lintian warning copyright-has-url-from-dh_make-boilerplate

  [ D Haley ]
  * Import rules include dir fix from 1.3.0-x series. (Closes: #619386)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 18 Feb 2012 15:36:02 +0100

libstxxl (1.3.1-1) unstable; urgency=low

  * New upstream release
  * Add doxygen-latex build-dep (Closes: #616262)

  [ Sylvestre Ledru ]
  * Remove myself from the uploaders
  * Switch to dpkg-source 3.0 (quilt) format

 -- D Haley <mycae@yahoo.com>  Mon, 11 Apr 2011 00:40:06 +0100

libstxxl (1.3.0-1) unstable; urgency=low

  * New upstream release

 -- D Haley <mycae@yahoo.com>  Tue, 10 Aug 2010 03:18:56 +1000

libstxxl (1.2.1-3) unstable; urgency=low

  * Standards-Version updated to version 3.8.4

  [ D Haley ]
  * Remove floatflt from tutorial latex file, as it is no longer in texlive
  * Added more files to clean.

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 10 Feb 2010 09:59:38 +0100

libstxxl (1.2.1-2) unstable; urgency=low

  [ D Haley ]
  * Correct copyright field in debian/copyright

  [ Sylvestre Ledru ]
  * Add myself to the uploader (which I should have been done since the first
    release)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 09 Jan 2010 11:27:08 +0100

libstxxl (1.2.1-1) unstable; urgency=low

  * Initial release (Closes: #540513)

 -- D Haley <mycae@yahoo.com>  Sat, 29 Aug 2009 20:12:15 +1000
